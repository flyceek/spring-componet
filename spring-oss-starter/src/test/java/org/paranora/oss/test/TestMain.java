package org.paranora.oss.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.paranora.oss.configuration.ApplicationConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestMain.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@ActiveProfiles("development")
public class TestMain {

    @Test
    public void test_a() {
        System.out.println("hello , i am paranora.");
        System.out.println("end");
    }

}
