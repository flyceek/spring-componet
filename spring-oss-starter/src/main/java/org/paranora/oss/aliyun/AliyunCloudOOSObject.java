package org.paranora.oss.aliyun;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.paranora.oss.OOSObject;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 13:58
 */
@Data
@EqualsAndHashCode(callSuper=false)
@SuperBuilder
public class AliyunCloudOOSObject extends OOSObject {
}
