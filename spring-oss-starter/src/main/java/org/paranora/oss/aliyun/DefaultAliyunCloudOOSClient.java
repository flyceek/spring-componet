package org.paranora.oss.aliyun;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.event.ProgressEvent;
import com.aliyun.oss.event.ProgressEventType;
import com.aliyun.oss.event.ProgressListener;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import org.paranora.oss.ClientAbs;
import org.paranora.oss.Credentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.function.Consumer;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 13:41
 */
public class DefaultAliyunCloudOOSClient extends ClientAbs<AliyunOOSCredentials, AliyunCloudOOSObject,AliyunCloudOOSUploadEvent> implements AliyunCloudOOSClient<AliyunOOSCredentials, AliyunCloudOOSObject,AliyunCloudOOSUploadEvent> {

    protected Logger logger = LoggerFactory.getLogger(DefaultAliyunCloudOOSClient.class);

    public DefaultAliyunCloudOOSClient(AliyunOOSCredentials c) {
        super(c);
    }


    protected <C extends Credentials> OSS createOssClient() {
        OSS ossClient = new OSSClientBuilder().build(this.credentials.getEndpoint(), this.credentials.getAccessKeyId(), this.credentials.getAccessKeySecret());
        return ossClient;
    }

    protected void releaseOssClient(OSS ossClient) {
        if (null != ossClient) {
            ossClient.shutdown();
        }
    }

    @Override
    public void upload(AliyunCloudOOSObject obj, Consumer<AliyunCloudOOSUploadEvent> event) throws RuntimeException {
        OSS ossClient = createOssClient();
        try {
            PutObjectRequest putObjectRequest = new PutObjectRequest(obj.getBucket(), obj.getName(), obj.getContent());
            if (!ObjectUtils.isEmpty(event)) {
                putObjectRequest.withProgressListener(new PutObjectProgressListener(obj.getContent().available(),event));
            }
            PutObjectResult putObjectResult = ossClient.putObject(putObjectRequest);
        } catch (OSSException oe) {
            logger.error("Caught an OSSException, which means your request made it to OSS, but was rejected with an error response for some reason.");
            logger.error("Error Message:" + oe.getErrorMessage());
            logger.error("Error Code:" + oe.getErrorCode());
            logger.error("Request ID:" + oe.getRequestId());
            logger.error("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            logger.error("Caught an ClientException, which means the client encountered a serious internal problem while trying to communicate with OSS, such as not being able to access the network.");
            logger.error("Error Message:" + ce.getMessage());
        } catch (IOException e) {
            logger.error("Error Message:" + e.getMessage());
        } finally {
            releaseOssClient(ossClient);
        }
    }

    @Override
    public Boolean exist(AliyunCloudOOSObject obj) throws RuntimeException {
        OSS ossClient = createOssClient();
        return ossClient.doesObjectExist(obj.getBucket(), obj.getName());
    }

    @Override
    public void delete(AliyunCloudOOSObject obj) throws RuntimeException {
        OSS ossClient = createOssClient();
        try {
            ossClient.deleteObject(obj.getBucket(),obj.getName());
        } catch (OSSException oe) {
            logger.error("Caught an OSSException, which means your request made it to OSS, but was rejected with an error response for some reason.");
            logger.error("Error Message:" + oe.getErrorMessage());
            logger.error("Error Code:" + oe.getErrorCode());
            logger.error("Request ID:" + oe.getRequestId());
            logger.error("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            logger.error("Caught an ClientException, which means the client encountered  a serious internal problem while trying to communicate with OSS, such as not being able to access the network.");
            logger.error("Error Message:" + ce.getMessage());
        } finally {
            releaseOssClient(ossClient);
        }
    }

    @Override
    public void downLoad(AliyunCloudOOSObject obj, Consumer<AliyunCloudOOSObject> saver) {

    }

    @Override
    public void list(AliyunCloudOOSObject obj, Consumer<AliyunCloudOOSObject> consumer) {

    }

    class PutObjectProgressListener implements ProgressListener {
        private long bytesWritten = 0;
        private long totalBytes = -1;
        private Consumer<AliyunCloudOOSUploadEvent> event;

        public PutObjectProgressListener(long totalBytes,Consumer<AliyunCloudOOSUploadEvent> event){
            this.totalBytes=totalBytes;
            this.event=event;
        }

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
            long bytes = progressEvent.getBytes();
            ProgressEventType eventType = progressEvent.getEventType();
            switch (eventType) {
                case TRANSFER_STARTED_EVENT:
                    break;
                case REQUEST_CONTENT_LENGTH_EVENT:
                    this.totalBytes = bytes;
                    break;
                case REQUEST_BYTE_TRANSFER_EVENT:
                    this.bytesWritten += bytes;
                    break;
                case TRANSFER_COMPLETED_EVENT:
                    break;
                case TRANSFER_FAILED_EVENT:
                    break;
                default:
                    break;
            }
            if(null!=this.event){
                AliyunCloudOOSUploadEvent uploadEvent=new AliyunCloudOOSUploadEvent(eventType,bytes);
                uploadEvent.setTotalBytes(this.totalBytes);
                uploadEvent.setBytesWritten(this.bytesWritten);
                event.accept(uploadEvent);
            }
        }
    }
}
