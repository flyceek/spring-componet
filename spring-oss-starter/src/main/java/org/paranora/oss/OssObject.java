package org.paranora.oss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.InputStream;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 11:30
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class OssObject {
    private String domain;
    private String bucket;
    private String name;
    private InputStream content;
}
