package org.paranora.oss.aliyun;

import com.aliyun.oss.event.ProgressEventType;

public class AliyunCloudOOSUploadEvent extends UploadEvent{
    public AliyunCloudOOSUploadEvent(ProgressEventType eventType) {
        super(eventType);
    }

    public AliyunCloudOOSUploadEvent(ProgressEventType eventType, long bytes) {
        super(eventType, bytes);
    }
}
