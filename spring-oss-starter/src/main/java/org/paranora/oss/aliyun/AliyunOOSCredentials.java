package org.paranora.oss.aliyun;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;
import org.paranora.oss.Credentials;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper=false)
public class AliyunOOSCredentials extends Credentials {
}
