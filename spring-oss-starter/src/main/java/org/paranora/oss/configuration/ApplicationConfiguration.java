package org.paranora.oss.configuration;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;


/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 9:47
 */

@AutoConfiguration
@ComponentScan({"org.paranora.oss.configuration"})
//@Import({AliyunOOSUploadFileConfiguration.class})
public class ApplicationConfiguration {


    @Bean("Paranora_Test_Object_String_Bean")
    public String paranora(){
        return new String("paranora is king.");
    }
}