package org.paranora.oss;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 11:27
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class Credentials {
    protected String bucket;
    protected String endpoint;
    protected String accessKeyId;
    protected String accessKeySecret;
    protected String domain;
    protected String httpProtocol;
}
