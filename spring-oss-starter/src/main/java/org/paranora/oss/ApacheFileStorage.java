package org.paranora.oss;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.AutoCloseInputStream;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

public class ApacheFileStorage implements FileStorage {
    @Override
    public void save(InputStream input, String fileDir, String fileName, Long fileSize, Boolean overlay) throws IOException {
        String fileFullPath = Paths.get(fileDir, fileName).toString();
        AutoCloseInputStream acis = new AutoCloseInputStream(input);
        byte[] bytes = IOUtils.toByteArray(acis);
        File file = new File(fileFullPath);
        if (file.exists() && fileSize == file.length() && !overlay) {
            return;
        }
        FileUtils.writeByteArrayToFile(file, bytes);
    }
}
