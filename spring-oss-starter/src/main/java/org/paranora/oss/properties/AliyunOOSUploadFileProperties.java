package org.paranora.oss.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class AliyunOOSUploadFileProperties extends OOSUploadFileProperties {

}
