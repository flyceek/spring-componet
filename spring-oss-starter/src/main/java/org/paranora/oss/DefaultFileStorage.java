package org.paranora.oss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

public class DefaultFileStorage implements FileStorage {

    private Logger logger = LoggerFactory.getLogger(DefaultFileStorage.class);

    @Override
    public void save(InputStream input, String fileDir, String fileName, Long fileSize, Boolean overlay) throws IOException {
        FileOutputStream fos = null;
        try {
            String fileFullPath = Paths.get(fileDir, fileName).toString();
            File file = new File(fileFullPath);
            if (file.exists() && fileSize == file.length() && !overlay) {
                return;
            }
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            fos = new FileOutputStream(file);
            byte[] buf = new byte[1024 * 5];
            while (true) {
                int read = 0;
                if (input != null) {
                    read = input.read(buf);
                }
                if (read == -1) {
                    break;
                }
                fos.write(buf, 0, read);
            }
            fos.flush();
            logger.info(String.format("save file succeed , file : %s .", fileName));
        } catch (IOException e) {
            logger.error(String.format("save file error , file : %s .", fileName));
            throw e;
        } finally {
            if (fos != null) {
                fos.close();
            }
        }
    }
}
