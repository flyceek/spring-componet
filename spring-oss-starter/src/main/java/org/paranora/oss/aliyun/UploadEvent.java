package org.paranora.oss.aliyun;

import com.aliyun.oss.event.ProgressEvent;
import com.aliyun.oss.event.ProgressEventType;
import org.paranora.oss.Event;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 11:42
 */
public class UploadEvent extends ProgressEvent implements Event {
    private long bytesWritten;
    private long totalBytes;

    public UploadEvent(ProgressEventType eventType) {
        super(eventType);
    }

    public UploadEvent(ProgressEventType eventType, long bytes) {
        super(eventType, bytes);
    }

    public long getBytesWritten() {
        return bytesWritten;
    }

    public void setBytesWritten(long bytesWritten) {
        this.bytesWritten = bytesWritten;
    }

    public long getTotalBytes() {
        return totalBytes;
    }

    public void setTotalBytes(long totalBytes) {
        this.totalBytes = totalBytes;
    }
}
