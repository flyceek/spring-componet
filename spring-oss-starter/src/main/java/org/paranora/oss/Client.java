package org.paranora.oss;

import java.util.function.Consumer;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 13:21
 */
public interface Client<C extends Credentials, O extends OOSObject, E extends Event> {

    void upload(O obj, Consumer<E> event) throws RuntimeException;

    Boolean exist(O obj) throws RuntimeException;

    void delete(O obj) throws RuntimeException;

    void downLoad(O obj,Consumer<O> saver);

    void list(O obj,Consumer<O> consumer);
}