package org.paranora.oss.aliyun;


import org.paranora.oss.Client;

public interface AliyunCloudOOSClient<C extends AliyunOOSCredentials,O extends AliyunCloudOOSObject,E extends AliyunCloudOOSUploadEvent> extends Client<C, O,E> {
}
