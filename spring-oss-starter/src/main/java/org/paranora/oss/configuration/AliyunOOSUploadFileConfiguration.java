package org.paranora.oss.configuration;

import org.paranora.oss.aliyun.AliyunOOSCredentials;
import org.paranora.oss.aliyun.DefaultAliyunCloudOOSClient;
import org.paranora.oss.command.AliyunOssUploadFileCommand;
import org.paranora.oss.properties.AliyunOOSUploadFileProperties;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 17:19
 */
@Profile("aliyunOOSUpload")
@Configuration
public class AliyunOOSUploadFileConfiguration {

    @Qualifier("uploadFile")
    @Bean("uploadFile")
    @ConfigurationProperties(prefix = "uploadfile")
    public AliyunOOSUploadFileProperties uploadFile(){
        AliyunOOSUploadFileProperties uploadFile = new AliyunOOSUploadFileProperties();
        return uploadFile;
    }

    @Bean
    @ConfigurationProperties(prefix = "alioos")
    public AliyunOOSCredentials aliyunOOSCredentials(){
        return AliyunOOSCredentials.builder()
                .domain("shvrdata.centanet.com")
                .endpoint("oss-cn-shanghai.aliyuncs.com")
                .bucket("sh123kanfang")
                .accessKeyId("LTAI5tLRHk9gd24zyQdWDaUF")
                .accessKeySecret("ZK8Pnm7gnfxHW0AlnvCVYPHpWNxkdz")
                .httpProtocol("https")
                .build();
    }

    @Order(0)
    @Bean
    public CommandLineRunner commandLineRunner(){
        return new AliyunOssUploadFileCommand();
    }

    @Bean
    public DefaultAliyunCloudOOSClient aliyunCloudOOSClient(AliyunOOSCredentials credentials){
        return new DefaultAliyunCloudOOSClient(credentials);
    }
}
