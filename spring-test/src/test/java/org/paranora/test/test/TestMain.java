package org.paranora.test.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.paranora.test.configuration.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileNotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestMain.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@EnableAutoConfiguration
@ActiveProfiles({"development"})
public class TestMain {

    protected Logger logger = LoggerFactory.getLogger(TestMain.class);


    @Test
    public void test_main_method_main_aa() throws FileNotFoundException {

        System.out.println("hello , i am paranora.");
        System.out.println("end");
    }

}
