package org.paranora.test.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.paranora.test.configuration.ApplicationConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileNotFoundException;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/15 9:17
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TestMain.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@EnableAutoConfiguration
@ActiveProfiles("development")
public class TestJasypt {


    @Test
    public void test_jasypt_mehtod_main_aa() throws FileNotFoundException {

        System.out.println("hello , i am paranora.");
        System.out.println("end");
    }
}
