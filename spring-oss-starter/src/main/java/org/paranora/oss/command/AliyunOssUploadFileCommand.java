package org.paranora.oss.command;

import com.aliyun.oss.event.ProgressEventType;
import org.paranora.oss.aliyun.AliyunCloudOOSObject;
import org.paranora.oss.aliyun.AliyunOOSCredentials;
import org.paranora.oss.aliyun.DefaultAliyunCloudOOSClient;
import org.paranora.oss.properties.AliyunOOSUploadFileProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;

/**
 * @author :  paranora
 * @description :  TODO
 * @date :  2022/6/10 17:12
 **/

/**
 java \
-Dspring.profiles.active=aliyunOOSUpload \
-Duploadfile.fileDir=d:\\  \
-Duploadfile.fileName=test \
-Duploadfile.fileType=rar \
-Duploadfile.fileKey=app/test.rar \
-Duploadfile.fileContentType=application/octet-stream \
-Duploadfile.transCode=false \
-Dffmpeg.enable=false \
-Dffmpeg.filterEnable=false \
-Dffmpeg.watermarkEnable=false \
-jar griffonEye.jar

 */
public class AliyunOssUploadFileCommand implements CommandLineRunner {

    protected Logger logger = LoggerFactory.getLogger(AliyunOssUploadFileCommand.class);

    @Autowired
    protected AliyunOOSUploadFileProperties uploadFileProperties;

    @Autowired
    protected AliyunOOSCredentials credentials;

    @Autowired
    protected DefaultAliyunCloudOOSClient client;

    @Override
    public void run(String... args) throws Exception {
        String fileFullPath = Paths.get(uploadFileProperties.getFileDir(), String.format("%s.%s", uploadFileProperties.getFileName(), uploadFileProperties.getFileType())).toString();

        File file = new File(fileFullPath);
        if (!file.exists()) {
            throw new RuntimeException(String.format("file : %s , is not exist!", fileFullPath));
        }

        AliyunCloudOOSObject obj = AliyunCloudOOSObject.builder()
                .bucket(credentials.getBucket())
                .name(uploadFileProperties.getFileKey())
                .domain(credentials.getDomain())
                .content(new FileInputStream(fileFullPath))
                .build();

        String fileOOSUrl = String.format("%s://%s/%s", credentials.getHttpProtocol(), credentials.getDomain(), uploadFileProperties.getFileKey());

        client.upload(obj, e -> {
            long bytes = e.getBytes();
            ProgressEventType eventType = e.getEventType();
            switch (eventType) {
                case TRANSFER_STARTED_EVENT:
                    logger.info("Start to upload......");
                    break;
                case REQUEST_CONTENT_LENGTH_EVENT:
                    logger.info(bytes + " bytes in total will be uploaded to OSS");
                    break;
                case TRANSFER_COMPLETED_EVENT:
                    logger.info("Succeed to upload, " + e.getBytesWritten() + " bytes have been transferred in total");
                    logger.info(String.format("file download address ：%s the end.", fileOOSUrl));
                    break;
                case TRANSFER_FAILED_EVENT:
                    logger.info("Failed to upload, " + e.getBytesWritten() + " bytes have been transferred");
                    break;
                default:
                    break;
            }
        });
        logger.info(String.format("power by paranora @ %s.", System.currentTimeMillis()));
    }
}
