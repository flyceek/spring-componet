package org.paranora.oss.properties;

import lombok.Data;

@Data
public class OOSUploadFileProperties {
    protected String fileType;
    protected String fileKey;
    protected String fileName;
    protected String fileDir;
    protected String fileContentType;

}
