package org.paranora.oss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class ClientAbs<C extends Credentials, O extends OOSObject, E extends Event> implements Client<C, O, E> {

    protected C credentials;

    private Logger logger = LoggerFactory.getLogger(ClientAbs.class);

    public ClientAbs(C c) {
        this.credentials=c;
    }

}
