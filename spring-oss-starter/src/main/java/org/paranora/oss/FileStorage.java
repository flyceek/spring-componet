package org.paranora.oss;

import java.io.IOException;
import java.io.InputStream;

public interface FileStorage {
   void save(InputStream input, String fileDir, String fileName, Long fileSize, Boolean overlay) throws IOException;
}
